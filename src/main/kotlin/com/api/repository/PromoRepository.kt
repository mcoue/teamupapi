package com.api.repository

import com.api.bean.Promo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface PromoRepository : JpaRepository<Promo, String> {
    override fun findById(id: String): Optional<Promo>
}