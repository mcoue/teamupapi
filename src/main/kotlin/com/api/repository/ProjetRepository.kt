package com.api.repository

import com.api.bean.Classe
import com.api.bean.Enseignant
import com.api.bean.Projet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ProjetRepository : JpaRepository<Projet, String> {
    override fun findById(id: String): Optional<Projet>

    fun findAllByEnseignants(enseignant: Enseignant): List<Projet>

    fun findAllByClasses(classe: Classe): List<Projet>
}