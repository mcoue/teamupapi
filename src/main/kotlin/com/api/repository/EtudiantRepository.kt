package com.api.repository

import com.api.bean.Classe
import com.api.bean.Etudiant
import com.api.bean.Groupe
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface EtudiantRepository : JpaRepository<Etudiant, String> {
    override fun findById(id: String): Optional<Etudiant>

    fun findAllByGroupes(groupe: Groupe): List<Etudiant>

    fun findAllByClasse(classe: Classe): List<Etudiant>

}