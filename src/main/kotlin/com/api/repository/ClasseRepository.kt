package com.api.repository

import com.api.bean.Classe
import com.api.bean.Projet
import com.api.bean.Promo
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface ClasseRepository : JpaRepository<Classe, String> {
    override fun findById(id: String): Optional<Classe>

    fun findAllByProjets(projet: Projet): List<Classe>

    fun findAllByPromo(promo: Promo): List<Classe>
}