package com.api.repository

import com.api.bean.Enseignant
import com.api.bean.Projet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface EnseignantRepository : JpaRepository<Enseignant, String> {
    override fun findById(id: String): Optional<Enseignant>

    fun findAllByProjets(projet: Projet): List<Enseignant>
}