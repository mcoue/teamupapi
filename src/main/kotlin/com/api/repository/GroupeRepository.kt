package com.api.repository

import com.api.bean.Etudiant
import com.api.bean.Groupe
import com.api.bean.Projet
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface GroupeRepository : JpaRepository<Groupe, String> {
    override fun findById(id: String): Optional<Groupe>
    fun findAllByEtudiants(etudiant: Optional<Etudiant>): List<Groupe>

    fun findAllByProjet(projet: Projet): List<Groupe>
}
