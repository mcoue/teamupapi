package com.api.bean

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import jakarta.persistence.*
import java.io.Serializable
import java.sql.Date

@Entity
@Table(name = "projet")
class Projet (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        var id: Int,

        @Column(name = "nom_projet", nullable = false)
        var nomProjet: String,

        @Column(name = "description", nullable = true)
        var description: String?,

        @Column(name = "taille_min", nullable = true)
        var tailleMin: Int?,

        @Column(name = "taille_max", nullable = true)
        var tailleMax: Int?,

        @Column(name = "date_debut", nullable = false)
        var dateDebut: Date,

        @Column(name = "date_fin", nullable = false)
        var dateFin: Date,

        @ManyToMany
        @JoinTable(
                name = "projet_enseignant",
                joinColumns = [JoinColumn(name = "id_projet")],
                inverseJoinColumns = [JoinColumn(name = "id_enseignant")]
        )
        @JsonIgnoreProperties("projets")
        var enseignants: MutableList<Enseignant> = mutableListOf(),

        @OneToMany(mappedBy = "projet")
        @JsonIgnoreProperties("projet")
        var groupes: MutableList<Groupe> = mutableListOf(),

        @ManyToMany
        @JoinTable(
                name = "classe_projet",
                joinColumns = [JoinColumn(name = "id_projet")],
                inverseJoinColumns = [JoinColumn(name = "id_classe")]
        )
        @JsonIgnoreProperties("projets")
        var classes: MutableList<Classe> = mutableListOf()
) : Serializable