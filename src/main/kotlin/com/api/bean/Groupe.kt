package com.api.bean

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import jakarta.persistence.*
import java.io.Serializable


@Entity
@Table(name = "groupe")
class Groupe (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        var id: Int,

        @Column(name = "nom", nullable = false)
        var nom: String,

        @Column(name = "taille", nullable = false)
        var taille: Int,

        @ManyToOne
        @JoinColumn(name = "id_projet")
        @JsonIgnoreProperties("groupes")
        var projet: Projet,

        @ManyToMany
        @JoinTable(
                name = "etudiant_groupe",
                joinColumns = [JoinColumn(name = "id_groupe")],
                inverseJoinColumns = [JoinColumn(name = "id_etudiant")]
        )
        @JsonIgnoreProperties("groupes")
        var etudiants: MutableList<Etudiant> = mutableListOf()
) : Serializable