package com.api.bean

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import jakarta.persistence.*
import java.io.Serializable
import java.sql.Date
import javax.swing.GroupLayout.Group

@Entity
@Table(name = "etudiant")
class Etudiant (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        var id: Int,

        @Column(name = "numero_etudiant", nullable = false)
        var numeroEtudiant: String,

        @Column(name = "nom", nullable = false)
        var nom: String,

        @Column(name = "prenom", nullable = false)
        var prenom: String,

        @Column(name = "date_naissance", nullable = false)
        var dateNaissance: Date,

        @Column(name = "mdp", nullable = true)
        var mdp: String?,

        @ManyToOne
        @JoinColumn(name = "id_classe")
        var classe: Classe,

        @ManyToMany(mappedBy = "etudiants")
        @JsonIgnoreProperties("etudiants", "classes", "enseignants", "projet")
        var groupes: MutableList<Groupe> = mutableListOf()
) : Serializable