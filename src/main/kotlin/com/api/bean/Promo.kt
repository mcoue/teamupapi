package com.api.bean

import jakarta.persistence.*
import java.io.Serializable

@Entity
@Table(name = "promo")
class Promo (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        var id: Int,

        @Column(name = "nom", nullable = false)
        var nom: String
) : Serializable