package com.api.bean

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import jakarta.persistence.*
import java.io.Serializable

@Entity
@Table(name = "classe")
class Classe (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        var id: Int,

        @Column(name = "nom", nullable = false)
        var nom: String,

        @ManyToOne
        @JoinColumn(name = "id_promo")
        var promo: Promo,

        @ManyToMany(mappedBy = "classes")
        @JsonIgnoreProperties("classes", "enseignants", "groupes", "etudiants")
        var projets: MutableList<Projet> = mutableListOf()
) : Serializable