package com.api.bean

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import jakarta.persistence.*
import java.io.Serializable
import java.sql.Date

@Entity
@Table(name = "enseignant")
class Enseignant (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        var id: Int,

        @Column(name = "nom", nullable = false)
        var nom: String,

        @Column(name = "prenom", nullable = false)
        var prenom: String,

        @Column(name = "dateNaissance", nullable = false)
        var dateNaissance: Date,

        @Column(name = "mdp", nullable = true)
        var mdp: String?,

        @ManyToMany
        @JoinTable(
                name = "projet_enseignant",
                joinColumns = [JoinColumn(name = "id_enseignant")],
                inverseJoinColumns = [JoinColumn(name = "id_projet")]
        )
        @JsonIgnoreProperties("enseignants", "groupes", "classes")
        var projets: MutableList<Projet> = mutableListOf()
) : Serializable