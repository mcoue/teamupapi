package com.api.controller

import com.api.bean.Classe
import com.api.bean.Etudiant
import com.api.bean.Groupe
import com.api.bean.Projet
import com.api.repository.EtudiantRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/etudiants")
class EtudiantControleur {
    @Autowired
    private lateinit var etudiantRepository: EtudiantRepository

    @GetMapping
    fun getAllEtudiants(): ResponseEntity<List<Etudiant>> {
        val etudiants = etudiantRepository.findAll()
        return ResponseEntity(etudiants, HttpStatus.OK)
    }

    @GetMapping("/{idEtudiant}")
    fun getEtudiant(@PathVariable idEtudiant: String): ResponseEntity<Etudiant> {
        val etudiant = etudiantRepository.findById(idEtudiant)
        return if (etudiant.isPresent) {
            ResponseEntity(etudiant.get(), HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idEtudiant}/classe")
    fun getClasseNumeroEtudiant(@PathVariable idEtudiant: String): ResponseEntity<Classe> {
        val etudiant = etudiantRepository.findById(idEtudiant)
        return if (etudiant.isPresent) {
            ResponseEntity(etudiant.get().classe, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idEtudiant}/projets")
    fun getProjetsNumeroEtudiant(@PathVariable idEtudiant: String): ResponseEntity<List<Projet>> {
        val etudiant = etudiantRepository.findById(idEtudiant)
        return if (etudiant.isPresent) {
            val projets = ArrayList<Projet>()
            for (groupe in etudiant.get().groupes) {
                if (!projets.contains(groupe.projet)) {
                    projets.add(groupe.projet)
                }
            }
            ResponseEntity(projets, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idEtudiant}/groupes")
    fun getGroupesNumeroEtudiant(@PathVariable idEtudiant: String): ResponseEntity<List<Groupe>> {
        val etudiant = etudiantRepository.findById(idEtudiant)
        return if (etudiant.isPresent) {
            ResponseEntity(etudiant.get().groupes, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping(consumes = ["application/json"])
    fun createEtudiant(@RequestBody etudiant: Etudiant): ResponseEntity<Etudiant> {
        val createdEtudiant = etudiantRepository.save(etudiant)
        return ResponseEntity(createdEtudiant, HttpStatus.CREATED)
    }

    @PutMapping("/{idEtudiant}",consumes = ["application/json"])
    fun updateEtudiant(@PathVariable idEtudiant: String, @RequestBody etudiant: Etudiant): ResponseEntity<Etudiant> {
        val existingEtudiant = etudiantRepository.findById(idEtudiant)
        return if (existingEtudiant.isPresent) {
            val updatedEtudiant = existingEtudiant.get().apply {
                numeroEtudiant = etudiant.numeroEtudiant
                nom = etudiant.nom
                prenom = etudiant.prenom
                dateNaissance = etudiant.dateNaissance
                mdp = etudiant.mdp
                classe = etudiant.classe
                groupes = etudiant.groupes
            }
            etudiantRepository.save(updatedEtudiant)
            ResponseEntity(updatedEtudiant, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @DeleteMapping("/{idEtudiant}")
    fun deleteEtudiant(@PathVariable idEtudiant: String): ResponseEntity<Unit> {
        val existingEtudiant = etudiantRepository.findById(idEtudiant)
        return if (existingEtudiant.isPresent) {
            etudiantRepository.deleteById(idEtudiant)
            ResponseEntity(HttpStatus.NO_CONTENT)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
}