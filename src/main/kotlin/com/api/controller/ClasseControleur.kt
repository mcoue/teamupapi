package com.api.controller

import com.api.bean.Classe
import com.api.bean.Projet
import com.api.repository.ClasseRepository
import com.api.repository.ProjetRepository
import com.api.repository.PromoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/classes")
class ClasseControleur {
    @Autowired
    private lateinit var classeRepository: ClasseRepository

    @Autowired
    private lateinit var projetRepository: ProjetRepository

    @GetMapping
    fun getAllClasses(): ResponseEntity<List<Classe>> {
        val classes = classeRepository.findAll()
        return ResponseEntity(classes, HttpStatus.OK)
    }

    @GetMapping("/{idClasse}")
    fun getClasse(@PathVariable idClasse: String): ResponseEntity<Classe> {
        val classe = classeRepository.findById(idClasse)
        return if (classe.isPresent) {
            ResponseEntity(classe.get(), HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idClasse}/projets")
    fun getEtudiantsClasse(@PathVariable idClasse: String): ResponseEntity<List<Projet>> {
        val classe = classeRepository.findById(idClasse)
        return if (classe.isPresent) {
            val projets = projetRepository.findAllByClasses(classe.get())
            if (projets.isNotEmpty()) {
                ResponseEntity.ok(projets)
            } else {
                ResponseEntity(HttpStatus.NOT_FOUND)
            }
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping(consumes = ["application/json"])
    fun post(@RequestBody classe: Classe): ResponseEntity<Classe> {
        val createdClasse = classeRepository.save(classe)
        return ResponseEntity(createdClasse, HttpStatus.CREATED)
    }

    @PutMapping("/{idClasse}",consumes = ["application/json"])
    fun updateClasse(@PathVariable idClasse: String, @RequestBody classe: Classe): ResponseEntity<Classe> {
        val existingClasse = classeRepository.findById(idClasse)
        return if (existingClasse.isPresent) {
            val classeToUpdate = existingClasse.get()
            classeToUpdate.nom = classe.nom
            classeToUpdate.promo = classe.promo
            classeToUpdate.projets = classe.projets
            val updatedClasse = classeRepository.save(classeToUpdate)
            ResponseEntity(updatedClasse, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @DeleteMapping("/{idClasse}")
    fun deleteClasse(@PathVariable idClasse: String): ResponseEntity<Unit> {
        val existingClasse = classeRepository.findById(idClasse)
        return if (existingClasse.isPresent) {
            classeRepository.deleteById(idClasse)
            ResponseEntity(HttpStatus.NO_CONTENT)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
}