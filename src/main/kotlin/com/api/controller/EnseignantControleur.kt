package com.api.controller

import com.api.bean.Enseignant
import com.api.bean.Projet
import com.api.repository.EnseignantRepository
import com.api.repository.ProjetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/enseignants")
class EnseignantControleur {
    @Autowired
    private lateinit var enseignantRepository: EnseignantRepository
    @Autowired
    private lateinit var projetRepository: ProjetRepository

    @GetMapping
    fun getAllEnseignants(): ResponseEntity<List<Enseignant>> {
        val enseignants = enseignantRepository.findAll()
        return ResponseEntity(enseignants, HttpStatus.OK)
    }

    @GetMapping("/{idEnseignant}")
    fun getEnseignant(@PathVariable idEnseignant: String): ResponseEntity<Enseignant> {
        val enseignant = enseignantRepository.findById(idEnseignant)
        return if (enseignant.isPresent) {
            ResponseEntity(enseignant.get(), HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idEnseignant}/projets")
    fun getProjetsEnseignant(@PathVariable idEnseignant: String) : ResponseEntity<List<Projet>> {
        val enseignant = enseignantRepository.findById(idEnseignant)
        return if (enseignant.isPresent) {
            val projets = projetRepository.findAllByEnseignants(enseignant.get())
            if (projets.isNotEmpty()) {
                ResponseEntity.ok(projets)
            } else {
                ResponseEntity(HttpStatus.NOT_FOUND)
            }
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping(consumes = ["application/json"])
    fun createEnseignant(@RequestBody enseignant: Enseignant): ResponseEntity<Enseignant> {
        val createdEnseignant = enseignantRepository.save(enseignant)
        return ResponseEntity(createdEnseignant, HttpStatus.CREATED)
    }

    @PutMapping("/{idEnseignant}", consumes = ["application/json"])
    fun updateEnseignant(@PathVariable idEnseignant: String, @RequestBody enseignant: Enseignant): ResponseEntity<Enseignant> {
        val existingEnseignant = enseignantRepository.findById(idEnseignant)
        return if (existingEnseignant.isPresent) {
            existingEnseignant.get().apply {
                nom = enseignant.nom
                prenom = enseignant.prenom
                dateNaissance = enseignant.dateNaissance
                mdp = enseignant.mdp
                projets = enseignant.projets
            }
            val updatedEnseignant = enseignantRepository.save(existingEnseignant.get())
            ResponseEntity(updatedEnseignant, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @DeleteMapping("/{idEnseignant}")
    fun deleteEnseignant(@PathVariable idEnseignant: String): ResponseEntity<Unit> {
        val existingEnseignant = enseignantRepository.findById(idEnseignant)
        return if (existingEnseignant.isPresent) {
            enseignantRepository.deleteById(idEnseignant)
            ResponseEntity(HttpStatus.NO_CONTENT)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
}