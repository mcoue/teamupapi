package com.api.controller

import com.api.bean.Etudiant
import com.api.bean.Groupe
import com.api.bean.Projet
import com.api.repository.EtudiantRepository
import com.api.repository.GroupeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/groupes")
class GroupeControleur {
    @Autowired
    private lateinit var groupeRepository: GroupeRepository
    @Autowired
    private lateinit var etudiantRepository: EtudiantRepository

    @GetMapping
    fun getAllGroupes(): ResponseEntity<List<Groupe>> {
        val groupes = groupeRepository.findAll()
        return ResponseEntity(groupes, HttpStatus.OK)
    }

    @GetMapping("/{idGroupe}")
    fun getGroupe(@PathVariable idGroupe: String): ResponseEntity<Groupe> {
        val groupe = groupeRepository.findById(idGroupe)
        return if (groupe.isPresent) {
            ResponseEntity(groupe.get(), HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idGroupe}/projet")
    fun getProjetGroupe(@PathVariable idGroupe: String): ResponseEntity<Projet> {
        val groupe = groupeRepository.findById(idGroupe)
        return if (groupe.isPresent) {
            ResponseEntity(groupe.get().projet, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idGroupe}/etudiants")
    fun getEtudiantsGroupe(@PathVariable idGroupe: String): ResponseEntity<List<Etudiant>> {
        val groupe = groupeRepository.findById(idGroupe)
        return if (groupe.isPresent) {
            val etudiant = etudiantRepository.findAllByGroupes(groupe.get())
            if (etudiant.isNotEmpty()) {
                ResponseEntity.ok(etudiant)
            } else {
                ResponseEntity(HttpStatus.NOT_FOUND)
            }
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping(consumes = ["application/json"])
    fun createGroupe(@RequestBody groupe: Groupe): ResponseEntity<Groupe> {
        val createdGroupe = groupeRepository.save(groupe)
        return ResponseEntity(createdGroupe, HttpStatus.CREATED)
    }

    @PutMapping("/{idGroupe}", consumes = ["application/json"])
    fun updateGroupe(@PathVariable idGroupe: String, @RequestBody groupe: Groupe): ResponseEntity<Groupe> {
        val existingGroupe = groupeRepository.findById(idGroupe)
        return if (existingGroupe.isPresent) {
            existingGroupe.get().apply {
                nom = groupe.nom
                taille = groupe.taille
                projet = groupe.projet
                etudiants = groupe.etudiants
            }

            val updatedGroupe = groupeRepository.save(existingGroupe.get())
            println("OUT " + updatedGroupe.etudiants)
            ResponseEntity(updatedGroupe, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @DeleteMapping("/{idGroupe}")
    fun deleteGroupe(@PathVariable idGroupe: String): ResponseEntity<Unit> {
        val existingGroupe = groupeRepository.findById(idGroupe)
        return if (existingGroupe.isPresent) {
            groupeRepository.deleteById(idGroupe)
            ResponseEntity(HttpStatus.NO_CONTENT)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
}