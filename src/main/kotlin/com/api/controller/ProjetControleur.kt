package com.api.controller

import com.api.bean.*
import com.api.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/projets")
class ProjetControleur {
    @Autowired
    private lateinit var projetRepository: ProjetRepository
    @Autowired
    private lateinit var groupeRepository: GroupeRepository
    @Autowired
    private lateinit var enseignantRepository: EnseignantRepository
    @Autowired
    private lateinit var etudiantRepository: EtudiantRepository
    @Autowired
    private lateinit var classeRepository: ClasseRepository

    @GetMapping
    fun getAllProjets(): ResponseEntity<List<Projet>> {
        val projets = projetRepository.findAll()
        return ResponseEntity(projets, HttpStatus.OK)
    }

    @GetMapping("/{idProjet}")
    fun getProjet(@PathVariable idProjet: String): ResponseEntity<Projet> {
        val projet = projetRepository.findById(idProjet)
        return if (projet.isPresent) {
            ResponseEntity(projet.get(), HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idProjet}/groupes")
    fun getGroupesProjet(@PathVariable idProjet: String): ResponseEntity<List<Groupe>> {
        val projet = projetRepository.findById(idProjet)
        return if (projet.isPresent) {
            val groupes = groupeRepository.findAllByProjet(projet.get())
            if (groupes.isNotEmpty()) {
                ResponseEntity.ok(groupes)
            } else {
                ResponseEntity(HttpStatus.NOT_FOUND)
            }
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idProjet}/enseignants")
    fun getEnseignantsProjet(@PathVariable idProjet: String) : ResponseEntity<List<Enseignant>> {
        val projet = projetRepository.findById(idProjet)
        return if (projet.isPresent) {
            val enseignants = enseignantRepository.findAllByProjets(projet.get())
            if (enseignants.isNotEmpty()) {
                ResponseEntity.ok(enseignants)
            } else {
                ResponseEntity(HttpStatus.NOT_FOUND)
            }
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idProjet}/etudiants")
    fun getEtudiantsProjet(@PathVariable idProjet: String): ResponseEntity<List<Etudiant>> {
        val projet = projetRepository.findById(idProjet)
        return if (projet.isPresent) {
            val groupes = groupeRepository.findAllByProjet(projet.get())
            val etudiants = ArrayList<Etudiant>()
            for (groupe in groupes) {
                etudiants.addAll(etudiantRepository.findAllByGroupes(groupe))
            }

            if (etudiants.isNotEmpty()) {
                ResponseEntity.ok(etudiants)
            } else {
                ResponseEntity(HttpStatus.NOT_FOUND)
            }
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @GetMapping("/{idProjet}/classes")
    fun getClassesProjet(@PathVariable idProjet: String) : ResponseEntity<List<Classe>> {
        val projet = projetRepository.findById(idProjet)
        return if (projet.isPresent) {
            val classes = classeRepository.findAllByProjets(projet.get())
            if (classes.isNotEmpty()) {
                ResponseEntity.ok(classes)
            } else {
                ResponseEntity(HttpStatus.NOT_FOUND)
            }
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping(consumes = ["application/json"])
    fun createProjet(@RequestBody projet: Projet): ResponseEntity<Projet> {
        val createdProjet = projetRepository.save(projet)
        return ResponseEntity(createdProjet, HttpStatus.CREATED)
    }

    @PutMapping("/{idProjet}", consumes = ["application/json"])
    fun updateProjet(@PathVariable idProjet: String, @RequestBody projet: Projet): ResponseEntity<Projet> {
        val existingProjet = projetRepository.findById(idProjet)
        return if (existingProjet.isPresent) {
            existingProjet.get().apply {
                nomProjet = projet.nomProjet
                description = projet.description
                tailleMin = projet.tailleMin
                tailleMax = projet.tailleMax
                dateDebut = projet.dateDebut
                dateFin = projet.dateFin
                enseignants = projet.enseignants
                groupes = projet.groupes
                classes = projet.classes
            }
            val updatedProjet = projetRepository.save(existingProjet.get())
            ResponseEntity(updatedProjet, HttpStatus.OK)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }

    @DeleteMapping("/{idProjet}")
    fun deleteProjet(@PathVariable idProjet: String): ResponseEntity<Unit> {
        val existingProjet = projetRepository.findById(idProjet)
        return if (existingProjet.isPresent) {
            projetRepository.deleteById(idProjet)
            ResponseEntity(HttpStatus.NO_CONTENT)
        } else {
            ResponseEntity(HttpStatus.NOT_FOUND)
        }
    }
}
